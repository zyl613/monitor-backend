package com.huatu.sba.monitorbackend;

import de.codecentric.boot.admin.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.List;

@SpringBootApplication
@EnableAdminServer
public class MonitorBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(MonitorBackendApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate(){
        RestTemplate restTemplate = new RestTemplate();
        // 使用 utf-8 编码集的 conver 替换默认的 conver（默认的 string conver 的编码集为"ISO-8859-1"）
        List<HttpMessageConverter<?>> converterList = restTemplate.getMessageConverters();
        for (HttpMessageConverter<?> item : converterList) {
            if (item.getClass() == StringHttpMessageConverter.class) {
                int index = converterList.indexOf(item);
                converterList.remove(index);
                HttpMessageConverter<?> converter = new StringHttpMessageConverter(StandardCharsets.UTF_8);
                converterList.add(index, converter);
                break;
            }
        }
        return restTemplate;
    }

}
